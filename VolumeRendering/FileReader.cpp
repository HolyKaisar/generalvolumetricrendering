#include "FileReader.h"


FileReader::FileReader()
{
}


FileReader::~FileReader()
{
}

void FileReader::LoadRawFile8Bit(const wstring file_path, float** data) {
	streampos size;
	char *buffer = nullptr;
	ifstream file(L"skull.raw", ios::in | ios::binary | ios::ate);
	if (file.is_open()) {
		size = file.tellg();
		buffer = new char[size];
		file.seekg(0, ios::beg);
		file.read(buffer, size);
		file.close();
		cout << "Binary file is fully loaded in the memory" << endl;

		// Scale the scalar values to [0, 1]
		(*data) = new float[size];
		int i = 0;
		for (i = 0; i < size; i++) (*data)[i] = buffer[i] / 255.0f;

		delete[] buffer;
	}
	else cout << "Unable to open file" << endl;
}
