#pragma once
#include "Header.h"
#include "Utilities.h"

class VolumeRayCasting
{
	struct ConstBuffer {
		XMMATRIX mWorld;
		XMMATRIX mView;
		XMMATRIX mProjection;
		XMFLOAT4 fEyePos;
	};
public:
	VolumeRayCasting();
	~VolumeRayCasting();

	/*
	* Initialization
	*/
	HRESULT Initialization(ID3D11Device* pd3dDevice, const wstring shader_file);

	/*
	* Clean up function
	*/
	void CleanUp();

	/*
	* Render the volumetric data
	*/
	void RenderVolumeData(ID3D11DeviceContext* pd3dImmediateContext, XMMATRIX &mWorld, XMMATRIX &mView, XMMATRIX &mProj, XMFLOAT4 &cameraPos);

	/*
	* Render the volumetric Cube
	*/
	void RenderVolumeCube(ID3D11DeviceContext* pd3dImmediateContext, XMMATRIX &mWorld, XMMATRIX &mView, XMMATRIX &mProj);

	/*
	* Load data into 3D texture
	*/
	HRESULT Load3DVolumeData(ID3D11Device* pd3dDevice, float* data, const unsigned int width, const unsigned int height, const unsigned int depth);

	// Get and Set methods
	ID3D11Texture3D* GetVolumeDataTex3D() { return m_pVolumeDataTex3D; }
	ID3D11ShaderResourceView* GetVolumeDataSRV3D() { return m_pSRV3D; }
	ID3D11ShaderResourceView* GetResultSRV() { return m_pSRV; }
	// TODO the following functions are temporary functions
	ID3D11ShaderResourceView* GetVolumeFrontSRV() { return m_pVolumeFrontSRV; }
	ID3D11ShaderResourceView* GetVolumeBackSRV() { return m_pVolumeBackSRV; }

private:
	struct SimpleVertex
	{
		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
	};

	HRESULT initializeVolumeCube(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext) {
		HRESULT hr = S_OK;
		// Compile the vertex shader
		ID3DBlob* pVSBlob = nullptr;
		wstring file_name = L"FacesProcessor.hlsl";
		V_RETURN(DXUTCompileFromFile(file_name.c_str(), nullptr, "VS", "vs_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pVSBlob));

		// Create the vertex shader
		hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &m_pCubeVertexShader);
		if (FAILED(hr)) {
			SAFE_RELEASE(pVSBlob);
			return hr;
		}

		// Define the input layout
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		UINT numElements = ARRAYSIZE(layout);

		// Create the input layout
		hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
			pVSBlob->GetBufferSize(), &m_pCubeInputLayout);
		SAFE_RELEASE(pVSBlob);
		if (FAILED(hr))
			return hr;

		// Compile the pixel shader
		ID3DBlob* pPSBlob = nullptr;
		V_RETURN(DXUTCompileFromFile(file_name.c_str(), nullptr, "PS", "ps_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pPSBlob));

		// Create the pixel shader
		hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pCubePixelShader);
		SAFE_RELEASE(pPSBlob);
		if (FAILED(hr))
			return hr;

		// Create vertex buffer
		SimpleVertex vertices[] =
		{
			{ XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) },
			{ XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },
			{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },
			{ XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },

			{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },
			{ XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) },
			{ XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
			{ XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },

			{ XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },
			{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) },
			{ XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) },
			{ XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) },

			{ XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
			{ XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) },
			{ XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },
			{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f) },

			{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) },
			{ XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) },
			{ XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) },
			{ XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },

			{ XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
			{ XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },
			{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) },
			{ XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f) },
		};

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(SimpleVertex)* 24;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = vertices;
		V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, &m_pCubeVertexBuffer));

		// Create index buffer
		DWORD indices[] =
		{
			3, 1, 0,
			2, 1, 3,

			6, 4, 5,
			7, 4, 6,

			11, 9, 8,
			10, 9, 11,

			14, 12, 13,
			15, 12, 14,

			19, 17, 16,
			18, 17, 19,

			22, 20, 21,
			23, 20, 22
		};

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(DWORD)* 36;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		InitData.pSysMem = indices;
		V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, &m_pCubeIndexBuffer));
	}

	void VolumeCubeSetUp(ID3D11DeviceContext* pd3dImmediateContext) {
		// Set the input layout
		pd3dImmediateContext->IASetInputLayout(m_pCubeInputLayout);
		// Set vertex buffer
		UINT stride = sizeof(SimpleVertex);
		UINT offset = 0;
		pd3dImmediateContext->IASetVertexBuffers(0, 1, &m_pCubeVertexBuffer, &stride, &offset);
		// Set index buffer
		pd3dImmediateContext->IASetIndexBuffer(m_pCubeIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		// Set primitive topology
		pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}
private:
	// For volume data
	ID3D11Texture3D* m_pVolumeDataTex3D;
	ID3D11ShaderResourceView* m_pSRV3D;

	unsigned int m_uVolumeX;
	unsigned int m_uVolumeY;
	unsigned int m_uVolumeZ;

	// For front face of the volume
	ID3D11Texture2D* m_pVolumeFrontFace;
	ID3D11ShaderResourceView* m_pVolumeFrontSRV;
	ID3D11RenderTargetView* m_pVolumeFrontRTV;

	// For back face of the volume
	ID3D11Texture2D* m_pVolumeBackFace;
	ID3D11ShaderResourceView* m_pVolumeBackSRV;
	ID3D11RenderTargetView* m_pVolumeBackRTV;

	D3D11_VIEWPORT m_viewPort;

	ID3D11InputLayout* m_pInputLayout;

	ID3D11RasterizerState* m_pRasterizerStateFrontFace;
	ID3D11RasterizerState* m_pRasterizerStateBackFace;

	ID3D11SamplerState* m_pSamplerLinear;
	
	ID3D11Texture2D* m_pTex2D;
	ID3D11ShaderResourceView* m_pSRV;
	ID3D11RenderTargetView* m_pRTV;
	
	ID3D11Texture2D* m_pDepthStencilTex2D;
	ID3D11DepthStencilState* m_pDepthStencilState;
	ID3D11DepthStencilView* m_pDepthStencilView;

	ID3D11Buffer* m_pConstBuffer;
	ConstBuffer m_ConstBuffer;

	ID3D11VertexShader* m_pVertexShader;
	ID3D11PixelShader* m_pPixelShader;
	ID3D11GeometryShader* m_pGeometryShader;
	ID3D11ComputeShader* m_pComputeShader;

	/***********************************************************/


	/***********************************************************/
	// For Volume Cube
	ID3D11VertexShader* m_pCubeVertexShader;
	ID3D11PixelShader* m_pCubePixelShader;
	ID3D11InputLayout* m_pCubeInputLayout;

	ID3D11Buffer* m_pCubeVertexBuffer;
	ID3D11Buffer* m_pCubeIndexBuffer;

public:
	/***********************************************************
	* TransferControlPoint Represents an RGBA value for 
	* a specific iso-value
	***********************************************************/
	class TransferControlPoint {
	public:
		TransferControlPoint(float r, float g, float b, int isovalue) {
			m_vColor4 = XMFLOAT4(r, g, b, 1.0f);
			m_iIsoValue = isovalue;
		}

		TransferControlPoint(float alpha, int isovalue) {
			m_vColor4 = XMFLOAT4(0.0f, 0.0f, 0.0f, alpha);
			m_iIsoValue = isovalue;
		}
		~TransferControlPoint() {};

		XMFLOAT4 m_vColor4;
		int m_iIsoValue;
	};

	class CubicSplineLine {
	public:
		CubicSplineLine(XMFLOAT4 tmp_a, XMFLOAT4 tmp_b, XMFLOAT4 tmp_c, XMFLOAT4 tmp_d) {
			m_vA = tmp_a;
			m_vB = tmp_b;
			m_vC = tmp_c;
			m_vD = tmp_d;
		}

		CubicSplineLine() {
			m_vA = m_vB = m_vC = m_vD = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
		}

		XMFLOAT4 GetPointOnSpline(float s) {
			//return (((m_vD * s) + m_vC) * s + m_vB) * s + m_vA;
			return XMFloat4Add(XMFloat4Scale(XMFloat4Add(XMFloat4Scale(XMFloat4Add(XMFloat4Scale(m_vD, s), m_vC), s), m_vB), s), m_vA);
		}

		void CalculateCubicSpline(int n, vector<TransferControlPoint> &vTCP, vector<CubicSplineLine>& res) {
			vector<XMFLOAT4> gamma(n + 1);
			vector<XMFLOAT4> delta(n + 1);
			vector<XMFLOAT4> D(n + 1);

			/* We need to solve the equation
			* taken from: http://mathworld.wolfram.com/CubicSpline.html
			[2 1       ] [D[0]]   [3(v[1] - v[0])  ]
			|1 4 1     | |D[1]|   |3(v[2] - v[0])  |
			|  1 4 1   | | .  | = |      .         |
			|    ..... | | .  |   |      .         |
			|     1 4 1| | .  |   |3(v[n] - v[n-2])|
			[       1 2] [D[n]]   [3(v[n] - v[n-1])]

			by converting the matrix to upper triangular.
			The D[i] are the derivatives at the control points.
			*/

			//this builds the coefficients of the left matrix
			gamma[0] = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
			gamma[0].x = 1.0f / 2.0f;
			gamma[0].y = 1.0f / 2.0f;
			gamma[0].z = 1.0f / 2.0f;
			gamma[0].w = 1.0f / 2.0f;

			for (int i = 1; i < n; i++) gamma[i] = XMFloat4Sub(XMFLOAT4(0.25f, 0.25f, 0.25f, 0.25f), gamma[i - 1]);
			gamma[n] = XMFloat4Sub(XMFLOAT4(0.5f, 0.5f, 0.5f, 0.5f), gamma[n - 1]);

			delta[0] = XMFloat4Mul(XMFloat4Scale(XMFloat4Sub(vTCP[1].m_vColor4, vTCP[0].m_vColor4), 3.0f), gamma[0]);
			for (int i = 1; i < n; i++) delta[i] = XMFloat4Mul(XMFloat4Sub(XMFloat4Scale(XMFloat4Sub(vTCP[i + 1].m_vColor4, vTCP[i - 1].m_vColor4), 3.0f), delta[i - 1]), gamma[i]);
			delta[n] = XMFloat4Mul(XMFloat4Sub(XMFloat4Scale(XMFloat4Sub(vTCP[n].m_vColor4, vTCP[n - 1].m_vColor4), 3.0f), delta[n - 1]), gamma[n]);

			D[n] = delta[n];
			for (int i = n - 1; i >= 0; i--) D[i] = XMFloat4Sub(delta[i], XMFloat4Mul(gamma[i], D[i + 1]));

			// Compute the coefficients of the cubicsplineline
			res.resize(n);
			for (int i = 0; i < n; i++) {
				res[i].m_vA = vTCP[i].m_vColor4;
				res[i].m_vB = D[i];
				res[i].m_vC = XMFloat4Sub(XMFloat4Sub(XMFloat4Scale(XMFloat4Sub(vTCP[i + 1].m_vColor4, vTCP[i].m_vColor4), 3.0f), XMFloat4Scale(D[i], 2)), D[i + 1]);
				res[i].m_vD = XMFloat4Add(XMFloat4Scale(XMFloat4Sub(vTCP[i].m_vColor4, vTCP[i + 1].m_vColor4), 2.0f), XMFloat4Add(D[i], D[i + 1]));
			}
		}
		
	private:
		XMFLOAT4 m_vA, m_vB, m_vC, m_vD;
	};

	HRESULT ComputeTransferFunction(ID3D11Device* pd3dDevice) {
		HRESULT hr = S_OK;
		// Initialize the cubi spline for the transfer function
		vector<XMFLOAT4> transferFunc(256);

		// Temporary transfer function copy the color/alpha from the
		// transfer control points
		vector<TransferControlPoint> tmpColorKnots(m_vColorKnots);
		vector<TransferControlPoint> tmpAlphaKnots(m_vAlphaKnots);

		vector<CubicSplineLine> colorCubicSplineLine;
		vector<CubicSplineLine> alphaCubicSplineLine;
		m_CubicSplineLine.CalculateCubicSpline(m_vColorKnots.size() - 1, tmpColorKnots, colorCubicSplineLine);
		m_CubicSplineLine.CalculateCubicSpline(m_vAlphaKnots.size() - 1, tmpAlphaKnots, alphaCubicSplineLine);

		int numTF = 0;
		for (unsigned int i = 0; i < m_vColorKnots.size() - 1; i++) {
			int steps = m_vColorKnots[i + 1].m_iIsoValue - m_vColorKnots[i].m_iIsoValue;

			for (int j = 0; j < steps; j++) {
				float k = (float)j / (float)(steps - 1);
				transferFunc[numTF++] = colorCubicSplineLine[i].GetPointOnSpline(k);
			}
		}

		numTF = 0;
		for (unsigned int i = 0; i < m_vAlphaKnots.size() - 1; i++) {
			int steps = m_vAlphaKnots[i + 1].m_iIsoValue - m_vAlphaKnots[i].m_iIsoValue;

			for (int j = 0; j < steps; j++) {
				float k = (float)j / (float)(steps - 1);

				transferFunc[numTF++].w = alphaCubicSplineLine[i].GetPointOnSpline(k).w;
			}
		}

		// Write to a transfer function texture
		float *pData = new float[4 * 256];
		memset(pData, 0, sizeof(float) * 4 * 256);

		for (int i = 0; i < 256; i++) {
			//XMFLOAT4 color = XMFloat4Scale(transferFunc[i], 255.0f);
			XMFLOAT4 color = transferFunc[i];
			pData[4 * i] = color.x;
			pData[4 * i + 1] = color.y;
			pData[4 * i + 2] = color.z;
			pData[4 * i + 3] = color.w;
		}

		for (int i = 0; i < 256; i++) {
			cout << pData[4 * i] << " " << pData[4 * i + 1] << " " << pData[4 * i + 2] << " " << pData[4 * i + 3] << endl;
		}

		system("pause");

		D3D11_SUBRESOURCE_DATA pInitData;
		pInitData.pSysMem = pData;
		pInitData.SysMemPitch = sizeof(float) * 4 * 256;
		pInitData.SysMemSlicePitch = 0;

		D3D11_TEXTURE1D_DESC RTtextureDesc;
		ZeroMemory(&RTtextureDesc, sizeof(D3D11_TEXTURE1D_DESC));
		RTtextureDesc.Width = 256;
		RTtextureDesc.MipLevels = 1;
		RTtextureDesc.ArraySize = 1;
		RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
		RTtextureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		RTtextureDesc.CPUAccessFlags = 0;
		RTtextureDesc.MiscFlags = 0;
		V_RETURN(pd3dDevice->CreateTexture1D(&RTtextureDesc, &pInitData, &m_pTransferSTex1D)); // For front texture2d

		D3D11_SHADER_RESOURCE_VIEW_DESC RtshaderResourceDesc;
		ZeroMemory(&RtshaderResourceDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		RtshaderResourceDesc.Format = RTtextureDesc.Format;
		RtshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
		RtshaderResourceDesc.Texture1D.MostDetailedMip = 0;
		RtshaderResourceDesc.Texture1D.MipLevels = 1;
		V_RETURN(pd3dDevice->CreateShaderResourceView(m_pTransferSTex1D, &RtshaderResourceDesc, &m_pTransferSSRV)); // For front srv

		delete[] pData;
		return hr;
	}

	HRESULT GenerateGradient(ID3D11Device* pd3dDevice, float* volumeData) {
		HRESULT hr = S_OK;

		float *gradients = new float[3 * 256 * 256 * 256];
		memset(gradients, 0, sizeof(float) * 3 * 256 * 256 * 256);
		int n = 1; // n is sampleSize
		XMFLOAT3 s1, s2;
		int index = 0;
		for (unsigned int z = 0; z < m_uVolumeZ; z++){
			for (unsigned int y = 0; y < m_uVolumeY; y++) {
				for (unsigned int x = 0; x < m_uVolumeX; x++) {
					s1.x = SampleVolume(x - n, y, z, volumeData);
					s2.x = SampleVolume(x + n, y, z, volumeData);
					s1.y = SampleVolume(x, y - n, z, volumeData);
					s2.y = SampleVolume(x, y + n, z, volumeData);
					s1.z = SampleVolume(x, y, z - n, volumeData);
					s2.z = SampleVolume(x, y, z + n, volumeData);

					gradients[index] = XMFloat3Normalize(XMFloat3Sub(s2, s1)).x;
					gradients[index + 1] = XMFloat3Normalize(XMFloat3Sub(s2, s1)).y;
					gradients[index + 2] = XMFloat3Normalize(XMFloat3Sub(s2, s1)).z;
					index += 3;
					if (isnan(gradients[index - 3])) {
						gradients[index - 3] = 0.0;
						gradients[index - 2] = 0.0;
						gradients[index - 1] = 0.0;
					}
				}
			}
		}

		// TODO filter the gradient to make it more smooth

		// TODO upload normals to the texture
		unsigned int volume_data_size = m_uVolumeX * m_uVolumeY * m_uVolumeZ;
		float* pData = new float[3 * volume_data_size];

		D3D11_SUBRESOURCE_DATA pInitData;
		pInitData.pSysMem = pData;
		pInitData.SysMemPitch = sizeof(float) * 3 * m_uVolumeX;
		pInitData.SysMemSlicePitch = 3 * m_uVolumeX * m_uVolumeY;

		D3D11_TEXTURE3D_DESC desc;
		ZeroMemory(&desc, sizeof(D3D11_TEXTURE3D_DESC));
		desc.Width = m_uVolumeX;
		desc.Height = m_uVolumeY;
		desc.Depth = m_uVolumeZ;
		desc.Format = DXGI_FORMAT_R32G32B32_FLOAT;
		desc.MipLevels = 1;
		desc.Usage = D3D11_USAGE_IMMUTABLE;
		desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		V_RETURN(pd3dDevice->CreateTexture3D(&desc, &pInitData, &m_pVolumeDataNormalsTex3D));

		D3D11_SHADER_RESOURCE_VIEW_DESC RtshaderResourceDesc;
		ZeroMemory(&RtshaderResourceDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		RtshaderResourceDesc.Format = desc.Format;
		RtshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
		RtshaderResourceDesc.Texture3D.MipLevels = desc.MipLevels;
		V_RETURN(pd3dDevice->CreateShaderResourceView(m_pVolumeDataNormalsTex3D, &RtshaderResourceDesc, &m_pVolumeDataNormalsSRV));

		delete[] pData;
	}

private:
	inline float SampleVolume(unsigned int x, unsigned int y, unsigned int z, float* volumeData) {
		x = Clamp<unsigned int>(x, 0, m_uVolumeX - 1);
		y = Clamp<unsigned int>(y, 0, m_uVolumeY - 1);
		z = Clamp<unsigned int>(z, 0, m_uVolumeZ - 1);

		return volumeData[x + (y * m_uVolumeX) + (z * m_uVolumeZ * m_uVolumeY)];
	}
private:
	vector<TransferControlPoint> m_vColorKnots;
	vector<TransferControlPoint> m_vAlphaKnots;

	CubicSplineLine m_CubicSplineLine;

	// Texture for transfer function
	ID3D11Texture1D* m_pTransferSTex1D;
	ID3D11ShaderResourceView* m_pTransferSSRV;

	// Texture for volume data normals
	ID3D11Texture3D* m_pVolumeDataNormalsTex3D;
	ID3D11ShaderResourceView* m_pVolumeDataNormalsSRV;
};