#include "VolumeRayCasting.h"


VolumeRayCasting::VolumeRayCasting() {
	m_pVolumeDataTex3D = NULL;
	m_pSRV3D = NULL;

	m_pVolumeFrontFace = NULL;
	m_pVolumeFrontSRV = NULL;
	m_pVolumeFrontRTV = NULL;


	m_pVolumeBackFace = NULL;
	m_pVolumeBackSRV = NULL;
	m_pVolumeBackRTV = NULL;


	m_pInputLayout = NULL;

	m_pRasterizerStateFrontFace = NULL;
	m_pRasterizerStateBackFace = NULL;

	m_pTex2D = NULL;
	m_pSRV = NULL;
	m_pRTV = NULL;

	m_pDepthStencilTex2D = NULL;
	m_pDepthStencilState = NULL;
	m_pDepthStencilView = NULL;

	m_pConstBuffer = NULL;

	m_pVertexShader = NULL;
	m_pPixelShader = NULL;
	m_pGeometryShader = NULL;
	m_pComputeShader = NULL;

	m_pCubeVertexShader = NULL;
	m_pCubePixelShader = NULL;

	m_pCubeInputLayout = NULL;

	m_pCubeVertexBuffer = NULL;
	m_pCubeIndexBuffer= NULL;
}


VolumeRayCasting::~VolumeRayCasting() {
}

HRESULT VolumeRayCasting::Initialization(ID3D11Device* pd3dDevice, const wstring shader_file) {
	HRESULT hr = S_OK;

	//////////////////////
	// Initialize shaders
	string vs_name = "VolumeRC_VS";
	//string ps_name = "VolumeRC_PS"; // For simple Ray Casting Volumetric Rendering
	string ps_name = "VolumeRCDiffuse_PS"; // For Ray Casting Volumetric Rendering with 1D Transfer Function
	string gs_name = "VolumeRC_GS";
	string cs_name = "VolumeRC_CS";

	// Load VS
	ID3DBlob* pVSBlob = NULL;
	V_RETURN(DXUTCompileFromFile(shader_file.c_str(), nullptr, vs_name.c_str(), "vs_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pVSBlob));
	V_RETURN(pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &m_pVertexShader));

	// Create input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	V_RETURN(pd3dDevice->CreateInputLayout(layout, ARRAYSIZE(layout), pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), &m_pInputLayout));
	pVSBlob->Release();

	// Load PS
	ID3DBlob* pPSBlob = NULL;
	V_RETURN(DXUTCompileFromFile(shader_file.c_str(), nullptr, ps_name.c_str(), "ps_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pPSBlob));
	V_RETURN(pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &m_pPixelShader));

	//////////////////////
	// Create Render Targets
	unsigned int m_rtvWidth = 800;
	unsigned int m_rtvHeight = 600;
	D3D11_TEXTURE2D_DESC RTtextureDesc = { 0 };
	RTtextureDesc.Width = m_rtvWidth;
	RTtextureDesc.Height = m_rtvHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	RTtextureDesc.CPUAccessFlags = 0;
	RTtextureDesc.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pTex2D));

	D3D11_SHADER_RESOURCE_VIEW_DESC RtshaderResourceDesc;
	RtshaderResourceDesc.Format = RTtextureDesc.Format;
	RtshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	RtshaderResourceDesc.Texture2D.MostDetailedMip = 0;
	RtshaderResourceDesc.Texture2D.MipLevels = 1;
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pTex2D, &RtshaderResourceDesc, &m_pSRV));

	D3D11_RENDER_TARGET_VIEW_DESC RTViewDesc;
	RTViewDesc.Format = RTtextureDesc.Format;
	RTViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTViewDesc.Texture2D.MipSlice = 0;
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pTex2D, &RTViewDesc, &m_pRTV));

	//////////////////////
	// Create Shader Resource View for front and back images of the volume
	ZeroMemory(&RTtextureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	RTtextureDesc.Width = m_rtvWidth;
	RTtextureDesc.Height = m_rtvHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	RTtextureDesc.CPUAccessFlags = 0;
	RTtextureDesc.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pVolumeFrontFace)); // For front texture2d
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pVolumeBackFace)); // For backs texture2d

	ZeroMemory(&RtshaderResourceDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	RtshaderResourceDesc.Format = RTtextureDesc.Format;
	RtshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	RtshaderResourceDesc.Texture2D.MostDetailedMip = 0;
	RtshaderResourceDesc.Texture2D.MipLevels = 1;
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pVolumeFrontFace, &RtshaderResourceDesc, &m_pVolumeFrontSRV)); // For front srv
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pVolumeBackFace, &RtshaderResourceDesc, &m_pVolumeBackSRV)); // For back srv

	ZeroMemory(&RTViewDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	RTViewDesc.Format = RTtextureDesc.Format;
	RTViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTViewDesc.Texture2D.MipSlice = 0;
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pVolumeFrontFace, &RTViewDesc, &m_pVolumeFrontRTV));
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pVolumeBackFace, &RTViewDesc, &m_pVolumeBackRTV));

	//////////////////////
	// Create Rasterizer State
	D3D11_RASTERIZER_DESC rasterizerState;
	rasterizerState.FillMode = D3D11_FILL_SOLID;
	rasterizerState.CullMode = D3D11_CULL_BACK;
	rasterizerState.FrontCounterClockwise = false;
	rasterizerState.DepthBias = false;
	rasterizerState.DepthBiasClamp = 0;
	rasterizerState.SlopeScaledDepthBias = 0;
	rasterizerState.DepthClipEnable = true;
	rasterizerState.ScissorEnable = false;
	rasterizerState.MultisampleEnable = true;
	rasterizerState.AntialiasedLineEnable = false;
	V_RETURN(pd3dDevice->CreateRasterizerState(&rasterizerState, &m_pRasterizerStateFrontFace)); // Rasterizer State that only show the fron face of the objects
	rasterizerState.FrontCounterClockwise = true;
	V_RETURN(pd3dDevice->CreateRasterizerState(&rasterizerState, &m_pRasterizerStateBackFace)); // Rasterizer state that only show the back face of the objects

	//////////////////////
	// Create View Port
	m_viewPort.Width = (float)m_rtvWidth;
	m_viewPort.Height = (float)m_rtvHeight;
	m_viewPort.MinDepth = 0.0;
	m_viewPort.MaxDepth = 1.0;
	m_viewPort.TopLeftX = 0.0;
	m_viewPort.TopLeftY = 0.0;

	//////////////////////
	// Create Constant Buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(ConstBuffer);
	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, &m_pConstBuffer));

	//////////////////////
	// Create SamplerState TODO may be I need to implement this in Load3DVlumeData
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	V_RETURN(pd3dDevice->CreateSamplerState(&sampDesc, &m_pSamplerLinear));

	//////////////////////
	// Create Stencil State (TODO do we need the stencil for 3D or just 2D at line 208
	ZeroMemory(&RTtextureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	RTtextureDesc.Width = m_rtvWidth;
	RTtextureDesc.Height = m_rtvHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.Format = DXUTGetDeviceSettings().d3d11.AutoDepthStencilFormat;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.SampleDesc.Quality = 0;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	RTtextureDesc.CPUAccessFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pDepthStencilTex2D));

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	descDSV.Format = RTtextureDesc.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	D3D11_DEPTH_STENCIL_DESC descDS;
	ZeroMemory(&descDS, sizeof(D3D11_DEPTH_STENCIL_DESC));
	// Depth test parameters initialization
	descDS.DepthEnable = true;
	descDS.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	descDS.DepthFunc = D3D11_COMPARISON_LESS;
	// Stencil test parameters initialization
	descDS.StencilEnable = true;
	descDS.StencilReadMask = 0xFF;
	descDS.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	descDS.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	descDS.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	descDS.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	descDS.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	descDS.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	descDS.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	descDS.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	descDS.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	V_RETURN(pd3dDevice->CreateDepthStencilState(&descDS, &m_pDepthStencilState));

	V_RETURN(pd3dDevice->CreateDepthStencilView(m_pDepthStencilTex2D, &descDSV, &m_pDepthStencilView));


	//////////////////////
	// Create Volume Box
	initializeVolumeCube(pd3dDevice, DXUTGetD3D11DeviceContext());

	return hr;
}

void VolumeRayCasting::CleanUp() {
	SAFE_RELEASE(m_pVolumeDataTex3D); 
	SAFE_RELEASE(m_pSRV3D); 

	SAFE_RELEASE(m_pVolumeFrontFace); 
	SAFE_RELEASE(m_pVolumeFrontSRV); 
	SAFE_RELEASE(m_pVolumeFrontRTV); 

	SAFE_RELEASE(m_pVolumeBackFace); 
	SAFE_RELEASE(m_pVolumeBackSRV); 
	SAFE_RELEASE(m_pVolumeBackRTV); 

	SAFE_RELEASE(m_pTex2D);
	SAFE_RELEASE(m_pSRV);
	SAFE_RELEASE(m_pRTV);

	SAFE_RELEASE(m_pDepthStencilTex2D);
	SAFE_RELEASE(m_pDepthStencilState);
	SAFE_RELEASE(m_pDepthStencilView);

	SAFE_RELEASE(m_pVertexShader);
	SAFE_RELEASE(m_pPixelShader);
	SAFE_RELEASE(m_pGeometryShader);
	SAFE_RELEASE(m_pComputeShader);

	SAFE_RELEASE(m_pInputLayout); 
	SAFE_RELEASE(m_pRasterizerStateFrontFace); 
	SAFE_RELEASE(m_pRasterizerStateBackFace); 

	SAFE_RELEASE(m_pConstBuffer);

	SAFE_RELEASE(m_pSamplerLinear);

	SAFE_RELEASE(m_pCubeVertexShader);
	SAFE_RELEASE(m_pCubePixelShader);

	SAFE_RELEASE(m_pCubeInputLayout);

	SAFE_RELEASE(m_pCubeVertexBuffer);
	SAFE_RELEASE(m_pCubeIndexBuffer);

	// Clean Transfer Function Resources
	SAFE_RELEASE(m_pTransferSTex1D);
	SAFE_RELEASE(m_pTransferSSRV);

	// Clear Volume Data Normal Resources
	SAFE_RELEASE(m_pVolumeDataNormalsTex3D);
	SAFE_RELEASE(m_pVolumeDataNormalsSRV);

	m_vColorKnots.clear();
	m_vAlphaKnots.clear();
}

HRESULT VolumeRayCasting::Load3DVolumeData(ID3D11Device* pd3dDevice, float* data, const unsigned int width, const unsigned int height, const unsigned int depth) {
	HRESULT hr = S_OK;
	// Store the volume dimension information
	m_uVolumeX = width;
	m_uVolumeY = height;
	m_uVolumeZ = depth;
	D3D11_TEXTURE3D_DESC desc;
	ZeroMemory(&desc, sizeof(D3D11_TEXTURE3D_DESC));
	desc.Width = width;
	desc.Height = height;
	desc.Depth = depth;
	//desc.Format = DXGI_FORMAT_R8_UNORM; // Might need this later
	desc.Format = DXGI_FORMAT_R32_FLOAT;
	desc.MipLevels = 1;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

	D3D11_SUBRESOURCE_DATA initialData;
	ZeroMemory(&initialData, sizeof(D3D11_SUBRESOURCE_DATA));
	initialData.pSysMem = data;
	initialData.SysMemPitch = width * 4;
	initialData.SysMemSlicePitch = width * height * 4;
	V_RETURN(pd3dDevice->CreateTexture3D(&desc, &initialData, &m_pVolumeDataTex3D));

	D3D11_SHADER_RESOURCE_VIEW_DESC RtshaderResourceDesc;
	ZeroMemory(&RtshaderResourceDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	RtshaderResourceDesc.Format = desc.Format;
	RtshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
	RtshaderResourceDesc.Texture3D.MipLevels = desc.MipLevels;
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pVolumeDataTex3D, &RtshaderResourceDesc, &m_pSRV3D));

	// Transfer function and gradient calculation
	m_vColorKnots.push_back(TransferControlPoint(0.91f, 0.7f, 0.61f, 0));
	m_vColorKnots.push_back(TransferControlPoint(0.91f, 0.7f, 0.61f, 40));
	m_vColorKnots.push_back(TransferControlPoint(1.00f, 1.0f, 0.85f, 42));
	m_vColorKnots.push_back(TransferControlPoint(1.00f, 1.0f, 0.85f, 256));

	m_vAlphaKnots.push_back(TransferControlPoint(0.0f, 0));
	m_vAlphaKnots.push_back(TransferControlPoint(0.0f, 40));
	m_vAlphaKnots.push_back(TransferControlPoint(0.2f, 60));
	m_vAlphaKnots.push_back(TransferControlPoint(0.05f, 63));
	m_vAlphaKnots.push_back(TransferControlPoint(0.0f, 80));
	m_vAlphaKnots.push_back(TransferControlPoint(0.9f, 82));
	m_vAlphaKnots.push_back(TransferControlPoint(1.0f, 256));
	ComputeTransferFunction(pd3dDevice); // Compute Transfer Function and store it in a texture

	//GenerateGradient(pd3dDevice, data); // Generate gradient and store it in a 3D texture;

	return hr;
}

void VolumeRayCasting::RenderVolumeData(ID3D11DeviceContext* pd3dImmediateContext, XMMATRIX &mWorld, XMMATRIX &mView, XMMATRIX &mProj, XMFLOAT4 &fCameraPos) {
	ID3D11RasterizerState* tmp;
	pd3dImmediateContext->RSGetState(&tmp);

	auto pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0, 0);
	pd3dImmediateContext->OMSetRenderTargets(1, &m_pRTV, pDSV);
	pd3dImmediateContext->ClearRenderTargetView(m_pRTV, Colors::MidnightBlue);
	pd3dImmediateContext->RSSetState(m_pRasterizerStateFrontFace);
	//
	// TODO ---- Vertex, index and input layout will be provided outside
	//
	m_ConstBuffer.mWorld = mWorld;
	m_ConstBuffer.mView = mView;
	m_ConstBuffer.mProjection = mProj;
	m_ConstBuffer.fEyePos = fCameraPos;
	pd3dImmediateContext->UpdateSubresource(m_pConstBuffer, 0, NULL, &m_ConstBuffer, 0, 0);

	pd3dImmediateContext->VSSetShader(m_pVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &m_pConstBuffer);

	pd3dImmediateContext->PSSetShader(m_pPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetShaderResources(0, 1, &m_pSRV3D);
	pd3dImmediateContext->PSSetShaderResources(1, 1, &m_pVolumeFrontSRV);
	pd3dImmediateContext->PSSetShaderResources(2, 1, &m_pVolumeBackSRV);
	pd3dImmediateContext->PSSetShaderResources(3, 1, &m_pTransferSSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &m_pSamplerLinear);
	pd3dImmediateContext->DrawIndexed(36, 0, 0);

	pd3dImmediateContext->RSSetState(tmp);

	ID3D11ShaderResourceView* nullTex[4] = { nullptr, nullptr, nullptr, nullptr };
	pd3dImmediateContext->PSSetShaderResources(4, 1, nullTex);
	SAFE_RELEASE(tmp);
}

void VolumeRayCasting::RenderVolumeCube(ID3D11DeviceContext* pd3dImmediateContext, XMMATRIX &mWorld, XMMATRIX &mView, XMMATRIX &mProj) {
	VolumeCubeSetUp(pd3dImmediateContext);

	ID3D11RasterizerState* tmp = NULL;
	pd3dImmediateContext->RSGetState(&tmp);

	auto pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0, 0);
	pd3dImmediateContext->OMSetRenderTargets(1, &m_pVolumeFrontRTV, pDSV);
	pd3dImmediateContext->ClearRenderTargetView(m_pVolumeFrontRTV, Colors::MidnightBlue);

	m_ConstBuffer.mWorld = mWorld;
	m_ConstBuffer.mView = mView;
	m_ConstBuffer.mProjection = mProj;
	m_ConstBuffer.fEyePos = XMFLOAT4(0.0, 0.0, 0.0, 0.0);
	pd3dImmediateContext->UpdateSubresource(m_pConstBuffer, 0, NULL, &m_ConstBuffer, 0, 0);

	pd3dImmediateContext->VSSetShader(m_pCubeVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &m_pConstBuffer);
	pd3dImmediateContext->PSSetShader(m_pCubePixelShader, nullptr, 0);

	pd3dImmediateContext->RSSetState(m_pRasterizerStateFrontFace); // Render Front Face
	pd3dImmediateContext->DrawIndexed(36, 0, 0);

	pd3dImmediateContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0, 0);
	pd3dImmediateContext->OMSetRenderTargets(1, &m_pVolumeBackRTV, pDSV);
	pd3dImmediateContext->ClearRenderTargetView(m_pVolumeBackRTV, Colors::MidnightBlue);
	pd3dImmediateContext->RSSetState(m_pRasterizerStateBackFace); // Render Back Face
	pd3dImmediateContext->DrawIndexed(36, 0, 0);

	pd3dImmediateContext->RSSetState(tmp);
	SAFE_RELEASE(tmp);
}