#include "MultiTextureRender.h"


MultiTextureRender::MultiTextureRender(UINT numOfTexture, bool bRenderToBackbuffer, UINT width, UINT height)
{
	m_uTextureNumber = numOfTexture;
	m_uTextureWidth = width;
	m_uTextureHeight = height;
	m_bDirectToBackBuffer = bRenderToBackbuffer;

	m_pInputTextureRV = new ID3D11ShaderResourceView*[m_uTextureNumber];
	m_pNullSRV = new ID3D11ShaderResourceView*[m_uTextureNumber];
	m_pVCamera = new CModelViewerCamera*[m_uTextureNumber];

	m_pVertexShader = NULL;
	m_pGeometryShader = NULL;
	m_pPixelShader = NULL;
	m_pInputLayout = NULL;
	m_pVertexBuffer = NULL;

	m_pGeneralTextureSS = NULL;

	m_pOutputTexture2D = NULL;
	m_pOutputTextureRTV = NULL;
	m_pOutputTextureRV = NULL;

	for (UINT i = 0; i < m_uTextureNumber; i++) {
		m_pInputTextureRV[i] = NULL;
		m_pNullSRV[i] = NULL;
		m_pVCamera[i] = NULL;
	}
}


MultiTextureRender::~MultiTextureRender()
{
}

HRESULT MultiTextureRender::Init()
{
	return S_OK;
}

void MultiTextureRender::ModifyDeviceSetting(DXUTDeviceSettings* pDeviceSettings)
{
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	int widthNum = 0, heightNum = 0;
	switch (m_uTextureNumber)
	{
	case 1:
		widthNum = 1; heightNum = 1; break;
	case 2:
		widthNum = 2; heightNum = 1; break;
	case 3:
		widthNum = 2; heightNum = 2; break;
	case 4:
		widthNum = 2; heightNum = 2; break;
	case 5:
		widthNum = 3; heightNum = 2; break;
	case 6:
		widthNum = 3; heightNum = 2; break;
	default:
		widthNum = 1; heightNum = 1;
	}

	sd.BufferCount = 1;
	sd.BufferDesc.Width = m_uTextureWidth * widthNum;
	sd.BufferDesc.Height = m_uTextureHeight * heightNum;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//sd.BufferDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = pDeviceSettings->d3d11.sd.OutputWindow;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = true;
	pDeviceSettings->d3d11.sd = sd;
}

HRESULT MultiTextureRender::CreateResource(ID3D11Device* pd3dDevice, vector<ID3D11ShaderResourceView*> shader_resource_view)
{
	HRESULT hr = S_OK;
	string GSName = "";

	if (shader_resource_view.size() > 4){
		MessageBox(NULL, L"The Maximum number of windows is 4!", L"Warning", MB_OK | MB_OKCANCEL);
	}

	for (UINT i = 0; i < m_uTextureNumber && i < shader_resource_view.size(); i++) {
		m_pInputTextureRV[i] = shader_resource_view[i];
	}

	m_uTextureNumber = min(m_uTextureNumber, (UINT)shader_resource_view.size());

	// Setup view port
	switch (m_uTextureNumber) {
	case 1:
		m_uRTwidth = m_uTextureWidth;
		m_uRTheight = m_uTextureHeight;
		GSName = "GS_1";
		break;
	case 2:
		m_uRTwidth = m_uTextureWidth * 2;
		m_uRTheight = m_uTextureHeight;
		GSName = "GS_2";
		break;
	case 3:
	case 4:
		m_uRTwidth = m_uTextureWidth * 2;
		m_uRTheight = m_uTextureHeight * 2;
		GSName = "GS_4";
		break;
	default:
		m_uRTwidth = m_uTextureWidth;
		m_uRTheight = m_uTextureHeight;
		GSName = "GS_1";
	}

	ID3DBlob *pVSBlob = NULL;
	wstring file_name = L"MultiTextureRenderShader.hlsl";

	// Load vs, gs and ps
	V_RETURN(DXUTCompileFromFile(file_name.c_str(), nullptr, "VS", "vs_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pVSBlob));
	V_RETURN(pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &m_pVertexShader));

	ID3DBlob* pGSBlob = NULL;
	V_RETURN(DXUTCompileFromFile(file_name.c_str(), nullptr, GSName.c_str(), "gs_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pGSBlob));
	V_RETURN(pd3dDevice->CreateGeometryShader(pGSBlob->GetBufferPointer(), pGSBlob->GetBufferSize(), NULL, &m_pGeometryShader));
	pGSBlob->Release();

	ID3DBlob* pPSBlob = NULL;
	V_RETURN(DXUTCompileFromFile(file_name.c_str(), nullptr, "PS", "ps_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pPSBlob));
	V_RETURN(pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &m_pPixelShader));
	pPSBlob->Release();

	D3D11_INPUT_ELEMENT_DESC layout[] = { { "POSITION", 0, DXGI_FORMAT_R16_SINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 } };
	V_RETURN(pd3dDevice->CreateInputLayout(layout, ARRAYSIZE(layout), pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), &m_pInputLayout));
	pVSBlob->Release();

	// Create the vertex buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(short);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	V_RETURN(pd3dDevice->CreateBuffer(&bd, NULL, &m_pVertexBuffer));

	// Create rendertarget resource
	if (!m_bDirectToBackBuffer) {
		D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
		RTtextureDesc.Width = m_uRTwidth;
		RTtextureDesc.Height = m_uRTheight;
		RTtextureDesc.MipLevels = 1;
		RTtextureDesc.ArraySize = 1;
		RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		//RTtextureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;

		RTtextureDesc.SampleDesc.Count = 1;
		RTtextureDesc.SampleDesc.Quality = 0;
		RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
		RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		RTtextureDesc.CPUAccessFlags = 0;
		RTtextureDesc.MiscFlags = 0;
		V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pOutputTexture2D));

		D3D11_RENDER_TARGET_VIEW_DESC		RTViewDesc;
		ZeroMemory(&RTViewDesc, sizeof(RTViewDesc));
		RTViewDesc.Format = RTtextureDesc.Format;
		RTViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		RTViewDesc.Texture2D.MipSlice = 0;
		V_RETURN(pd3dDevice->CreateRenderTargetView(m_pOutputTexture2D, &RTViewDesc, &m_pOutputTextureRTV));

		D3D11_SHADER_RESOURCE_VIEW_DESC		SRViewDesc;
		ZeroMemory(&SRViewDesc, sizeof(SRViewDesc));
		SRViewDesc.Format = RTtextureDesc.Format;
		SRViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		SRViewDesc.Texture2D.MostDetailedMip = 0;
		SRViewDesc.Texture2D.MipLevels = 1;
		V_RETURN(pd3dDevice->CreateShaderResourceView(m_pOutputTexture2D, &SRViewDesc, &m_pOutputTextureRV));
	}

	m_RTviewport.Width = (float)m_uRTwidth;
	m_RTviewport.Height = (float)m_uRTheight;
	m_RTviewport.MinDepth = 0.0f;
	m_RTviewport.MaxDepth = 1.0f;
	m_RTviewport.TopLeftX = 0.0f;
	m_RTviewport.TopLeftY = 0.0f;

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	V_RETURN(pd3dDevice->CreateSamplerState(&sampDesc, &m_pGeneralTextureSS));

	return hr;
}

void MultiTextureRender::CleanUp()
{
	if (m_pInputTextureRV) {
		for (int i = 0; i < m_uTextureNumber; i++){
			//m_pInputTextureRV[i]->Release();
			SAFE_RELEASE(m_pInputTextureRV[i]);
		}
		delete[] m_pInputTextureRV;
		m_pInputTextureRV = NULL;
	}

	if (m_pNullSRV) {
		delete[] m_pNullSRV;
		m_pNullSRV = NULL;
	}

	if (m_pVCamera) {
		delete[] m_pVCamera;
		m_pVCamera = NULL;
	}

	SAFE_RELEASE(m_pVertexShader);
	SAFE_RELEASE(m_pGeometryShader);
	SAFE_RELEASE(m_pPixelShader);
	SAFE_RELEASE(m_pInputLayout);
	SAFE_RELEASE(m_pVertexBuffer);
	SAFE_RELEASE(m_pGeneralTextureSS);
	if (!m_bDirectToBackBuffer) {
		SAFE_RELEASE(m_pOutputTexture2D);
		SAFE_RELEASE(m_pOutputTextureRV);
		SAFE_RELEASE(m_pOutputTextureRTV);
	}
}

void MultiTextureRender::LinkVCamera(vector<CModelViewerCamera*> cameras)
{
	for (UINT i = 0; i < m_uTextureNumber && i < cameras.size(); i++) {
		m_pVCamera[i] = cameras[i];
	}
}

void MultiTextureRender::SetupPipeline(ID3D11DeviceContext *pd3dImmediateContext)
{
	pd3dImmediateContext->OMSetRenderTargets(1, &m_pOutputTextureRTV, NULL);
	pd3dImmediateContext->IASetInputLayout(m_pInputLayout);
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	UINT stride = 0;
	UINT offset = 0;
	pd3dImmediateContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pd3dImmediateContext->VSSetShader(m_pVertexShader, NULL, 0);
	pd3dImmediateContext->GSSetShader(m_pGeometryShader, NULL, 0);
	pd3dImmediateContext->PSSetShader(m_pPixelShader, NULL, 0);
	pd3dImmediateContext->PSSetShaderResources(0, m_uTextureNumber, m_pInputTextureRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &m_pGeneralTextureSS);
	pd3dImmediateContext->RSSetViewports(1, &m_RTviewport);

	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	pd3dImmediateContext->ClearRenderTargetView(m_pOutputTextureRTV, ClearColor);
}

void MultiTextureRender::Resize()
{
	if (m_uTextureNumber <= 4) {
		for (int i = 0; i < m_uTextureNumber; i++) {
			if (m_pVCamera[i] != NULL) {
				RECT rc;
				rc.top = i / 2 * m_uTextureHeight;
				rc.bottom = (i / 2 + 1)*m_uTextureHeight;
				rc.left = i % 2 * m_uTextureWidth;
				rc.right = (i % 2 + 1)*m_uTextureWidth;
				m_pVCamera[i]->SetDragRect(rc);
			}
		}
	}
	//else
	//{
	//	for (int i = 0; i<6; i++)
	//	{
	//		if (m_pVCamera[i] != NULL)
	//		{
	//			RECT rc;
	//			rc.top = i / 3 * m_uTextureHeight;
	//			rc.bottom = (i / 3 + 1)*m_uTextureHeight;
	//			rc.left = i % 3 * m_uTextureWidth;
	//			rc.right = (i % 3 + 1)*m_uTextureWidth;
	//			m_pVCamera[i]->SetDragRect(rc);
	//		}
	//	}
	//}

	if (m_bDirectToBackBuffer)
	{
		m_pOutputTextureRTV = DXUTGetD3D11RenderTargetView();
	}
}

void MultiTextureRender::Update()
{
	// TODO User may change the number of windows during the run time
}

void MultiTextureRender::Render(ID3D11DeviceContext* pd3dImmediateContext)
{
	SetupPipeline(pd3dImmediateContext);
	pd3dImmediateContext->Draw(m_uTextureNumber, 0);
	pd3dImmediateContext->PSSetShaderResources(0, m_uTextureNumber, m_pNullSRV);
}