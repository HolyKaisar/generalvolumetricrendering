
Texture3D txVolumeData : register(t0);
Texture2D txFrontFace : register(t1);
Texture2D txBackFace : register(t2);
Texture1D txTransferS : register(t3);
Texture3D txVolumeDataNormal : register(t4);

SamplerState samLinear : register(s0);

cbuffer cbChangesEveryFrame : register(b0)
{
	matrix mWorld;
	matrix mView;
	matrix mProj;
	float4 fEyePpos;
};

struct VS_INPUT
{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float4 FinalPos : TEXCOORD1;
};

PS_INPUT VolumeRC_VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	//input.Pos -= 0.5;
	output.Pos = mul(float4(input.Pos, 1.0f), mWorld);
	output.Tex = input.Tex;
	output.FinalPos = output.Pos;
	return output;
}

/*
* Perform a per pixel ray casting volumetric rendering
*/
float4 VolumeRC_PS(PS_INPUT input) : SV_Target
{
	float StepSize = 0.02;
	int Iterations = 100;
	float2 texC = input.FinalPos.xy /= input.FinalPos.w;
	texC.x = 0.5f * texC.x + 0.5f;
	texC.y = -0.5f * texC.y + 0.5f;

	float3 front = txFrontFace.Sample(samLinear, texC).xyz;
	float3 back = txBackFace.Sample(samLinear, texC).xyz;

	float3 dir = normalize(back - front);
	float4 pos = float4(front, 0.0);

	float4 dst = float4(0.0, 0.0, 0.0, 0.0);
	float4 src = 0;

	float value = 0.0;
	float3 Step = dir * StepSize;

	for (int i = 0; i < Iterations; i++) {
		pos.w = 0;
		value = txVolumeData.Sample(samLinear, pos).r; // Get the scalar 

		src = (float4)value;
		src.a *= 0.5f; // Reduce the alpha to have a more transparent result
					   // this needs to be adjusted based on the step size
					   // i.e. the more steps we take, the faster the alpha will grow

		//Front to back blending
		// dst.rgb = dst.rgb + (1 - dst.a) * src.a * src.rgb
		// dst.a   = dst.a   + (1 - dst.a) * src.a	
		src.rgb *= src.a;
		dst = (1.0f - dst.a) * src + dst;

		if (dst.a >= 0.95f) break;

		// Advance the current position
		pos.xyz += Step;

		// Break if the position if greater tha, <1,1,1>
		if (pos.x > 1.0f || pos.y > 1.0f || pos.z > 1.0f) break;
	}

	return dst;
}

/*
* Perform a per pixel ray casting volumetric rendering with proper shading using Transfer function
*/
float4 VolumeRCDiffuse_PS(PS_INPUT input) : SV_Target
{
	float StepSize = 0.01;
	int Iterations = 200;
	float BaseSampleDist = .9f;
	float ActualSampleDist = .2f;

	float2 texC = input.FinalPos.xy /= input.FinalPos.w; // TODO need to figure out what is projection texture coordinates
	texC.x = 0.5f * texC.x + 0.5f;
	texC.y = -0.5f * texC.y + 0.5f;

	float3 front = txFrontFace.Sample(samLinear, texC).xyz;
	float3 back = txBackFace.Sample(samLinear, texC).xyz;
	float3 dir = normalize(back - front);
	float4 pos = float4(front, 0.0f);

	float4 dst = 0;
	float4 src = 0;

	float4 value = 0;
	float3 L = float3(0, 1, 1);

	float3 Step = dir * StepSize;

	for (int i = 0; i < Iterations; i++) {
		pos.w = 0;

		// Get the iso-values
		value = txVolumeData.Sample(samLinear, pos).r;
		// TODO get the normal for corresponding iso-value
		//float3 iso_normal = txVolumeDataNormal.Sample(samLinear, pos);// TODO use fake normal first
		float3 iso_normal = normalize(pos - float4(0.5, 0.5, 0.5, 0.0));

		// index the transfer function with the iso-value and get the rgba value for the voxel
		src = txTransferS.Sample(samLinear, value);

		//Oppacity correction: As sampling distance decreases we get more samples.
		//Therefore the alpha values set for a sampling distance of .5f will be too
		//high for a sampling distance of .25f (or conversely, too low for a sampling
		//distance of 1.0f). So we have to adjust the alpha accordingly.
		src.a = 1 - pow((1 - src.a), ActualSampleDist / BaseSampleDist);

		//float s = dot(iso_normal, L);
		// diffuse shading + fake ambient lighting
		//src.rgb = s * src.rgb + 0.6f * src.rgb;

		// Front to back blending
		src.rgb *= src.a;
		dst = (1.0f - dst.a) * src + dst;

		if (dst.a >= 0.95f) break;

		// Advance the current position
		pos.xyz += Step;

		if (pos.x > 1.0f || pos.y > 1.0f || pos.z > 1.0f) break;
	}

	return dst;
}