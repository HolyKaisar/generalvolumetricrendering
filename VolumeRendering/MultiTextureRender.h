
#pragma once
#include "Header.h"

class MultiTextureRender
{
public:
	MultiTextureRender(UINT numOfTexture = 1, bool bRenderToBackbuffer = true, UINT width = 640, UINT height = 480);
	~MultiTextureRender();

	HRESULT Init();
	HRESULT CreateResource(ID3D11Device* pd3dDevice, vector<ID3D11ShaderResourceView*> shader_resource_view);
	void	ModifyDeviceSetting(DXUTDeviceSettings* pDeviceSettings);
	void    CleanUp();
	void    LinkVCamera(vector<CModelViewerCamera*> cameras);
	void    SetupPipeline(ID3D11DeviceContext* pd3dImmediateContext);
	void    Resize();
	void    Update();
	void    Render(ID3D11DeviceContext* pd3dImmediateContext);

	LRESULT HandleMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
		UNREFERENCED_PARAMETER(lParam);
		UNREFERENCED_PARAMETER(hWnd);

		return 0;
	}

private:
	ID3D11VertexShader				*m_pVertexShader;
	ID3D11GeometryShader			*m_pGeometryShader;
	ID3D11PixelShader				*m_pPixelShader;
	ID3D11InputLayout				*m_pInputLayout;
	ID3D11Buffer					*m_pVertexBuffer;

	ID3D11ShaderResourceView        **m_pInputTextureRV;
	ID3D11ShaderResourceView		**m_pNullSRV;
	ID3D11SamplerState				*m_pGeneralTextureSS;

	ID3D11Texture2D					*m_pOutputTexture2D;
	ID3D11RenderTargetView			*m_pOutputTextureRTV;
	ID3D11ShaderResourceView		*m_pOutputTextureRV;

	D3D11_VIEWPORT					m_RTviewport;

	CModelViewerCamera				**m_pVCamera;

	UINT							m_uTextureWidth;
	UINT							m_uTextureHeight;
	UINT							m_uRTwidth;
	UINT							m_uRTheight;

	bool							m_bDirectToBackBuffer;
	UINT							m_uTextureNumber;
	UINT							m_uLayoutStyle;//1,2,4,6 subWindows
};

