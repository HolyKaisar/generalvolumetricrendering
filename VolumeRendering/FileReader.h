#pragma once
#include "Header.h"

class FileReader
{
public:
	FileReader();
	~FileReader();

	void LoadRawFile8Bit(const wstring file_path, float** data);
};

