// C++ common headers
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <limits.h>
#include <cstring>
#include <vector>

// Libraries for showing console in win32 application
#include <stdio.h>
#include <io.h>
#include <fcntl.h>

using namespace std;

// DXUT headers
#include "DXUT.h"
#include "DXUTgui.h"
#include "DXUTcamera.h"
#include "DXUTsettingsdlg.h"
#include "SDKmisc.h"

// D3D headers
#include <D3D11.h>
#include <DirectXMath.h>

using namespace DirectX;
