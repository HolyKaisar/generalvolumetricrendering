#pragma once
#include "Header.h"

/*
* Utility Functions
*/
// Other Operators
template<class T>
inline T Clamp(const T src, const T lower, const T upper) {
	return max(lower, min(src, upper));
}
// XMFLOAT4 Operators
inline XMFLOAT4 XMFloat4Scale(XMFLOAT4& src, float Scaler) {
	return XMFLOAT4(src.x * Scaler, src.y * Scaler, src.z * Scaler, src.w * Scaler);
}
inline XMFLOAT4 XMFloat4Mul(XMFLOAT4& src, XMFLOAT4& src2) {
	return XMFLOAT4(src.x * src2.x, src.y * src2.y, src.z * src2.z, src.w * src2.z);
}
inline XMFLOAT4 XMFloat4Add(XMFLOAT4& src1, XMFLOAT4& src2) {
	return XMFLOAT4(src1.x + src2.x, src1.y + src2.y, src1.z + src2.z, src1.w + src2.w);
}
inline XMFLOAT4 XMFloat4Sub(XMFLOAT4& minuend, XMFLOAT4& subtrahend) { // Peroform minuend - subtrahend e.g. (a - b)
	return XMFLOAT4(minuend.x - subtrahend.x, minuend.y - subtrahend.y, minuend.z - subtrahend.z, minuend.w - subtrahend.w);
}
// XMFLOAT3 Operators
inline XMFLOAT3 XMFloat3Normalize(XMFLOAT3 &src) {
	float mag = sqrt(src.x * src.x + src.y * src.y + src.z * src.z);
	return XMFLOAT3(src.x / mag, src.y / mag, src.z / mag);
}
inline XMFLOAT3 XMFloat3Sub(XMFLOAT3 &minuend, XMFLOAT3 &subtrahend) {
	return XMFLOAT3(minuend.x - subtrahend.x, minuend.y - subtrahend.y, minuend.z - subtrahend.z);
}
