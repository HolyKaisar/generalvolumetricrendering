//--------------------------------------------------------------------------------------
// File: Tutorial08.fx
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
Texture2D txDiffuse : register(t0);
Texture2D txDiffuse2 : register(t1);
SamplerState samLinear : register( s0 );

cbuffer cbChangesEveryFrame : register( b0 )
{
    matrix WorldViewProj;
    matrix World;
    float4 vMeshColor;
};


//--------------------------------------------------------------------------------------
struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
	float4 OriginalPos : TEXCOORD1;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output = (PS_INPUT)0;
	//input.Pos -= 0.5;
    output.Pos = mul( input.Pos, WorldViewProj );
    output.Tex = input.Tex;
	output.OriginalPos = output.Pos;

    return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( PS_INPUT input) : SV_Target
{
	float2 texC = input.OriginalPos.xy /= input.OriginalPos.w;
	texC.x = 0.5f * texC.x + 0.5f;
	texC.y = -0.5f * texC.y + 0.5f;

	float3 front = txDiffuse.Sample(samLinear, texC).xyz;
	float3 back = txDiffuse2.Sample(samLinear, texC).xyz;
	float3 dir = back - front;

	return float4(dir, 1.0);
}
